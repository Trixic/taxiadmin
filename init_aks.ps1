$location = "northeurope"
$rgName = "AksCluster"

Set-AzContext -Subscription "b87aace7-d8f7-4ff7-9afe-5be674f84c91"

New-AzResourceGroup -Name $rgName -Location $location

$aks = New-AzAksCluster -ResourceGroupName $rgName -Name AksCluster -NodeCount 1 -AddOnNameToBeEnabled HttpApplicationRouting -SshKeyValue 'C:\Users\Trixic\.ssh\id_rsa.pub'  -NodeVmSize Standard_B2s -NetworkPlugin azure
New-AzAksNodePool -ResourceGroupName $rgName -ClusterName AksCluster -VmSize Standard_B2s -OsType Windows -Name win -VmSetType VirtualMachineScaleSets -Count 1
Import-AzAksCredential -ResourceGroupName $rgName -Name AksCluster

$acr = New-AzContainerRegistry -ResourceGroupName $rgName -Name AcrForKuber -Sku Basic
Update-AzContainerRegistry -ResourceGroupName $rgName -Name AcrForKuber -EnableAdminUser
$acrCred = Get-AzContainerRegistryCredential -ResourceGroupName $rgName -Name AcrForKuber

$aks | Set-AzAksCluster -AcrNameToAttach acrforkuber

#set deployment acr url

# windows images built locally on win10 don't work in AKS
docker tag a40c3cc0db97 acrforkuber.azurecr.io/taxiadmin:latest
docker image push acrforkuber.azurecr.io/taxiadmin:latest

# need to build in ACR Tasks. 
# can be error - Windows version 10.0.20348-based image is incompatible ...
# AKS host isn't on newest version. Use older base image in Dockerfile
az acr build --registry acrforkuber --image taxiadmin2019:3.0 --platform windows .

helm upgrade --install --create-namespace --atomic --wait --namespace default taxiadmin ./kubernetes/taxiadmin #--set image.repository=${{ secrets.ACR_NAME }} --set dns.name=${{ secrets.DNS_NAME }}