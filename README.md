# Taxi Admin
Fleet management system

[http://taxiadmin.azurewebsites.net/](http://taxiadmin.azurewebsites.net/)

### Features

- View aggregated mileage data extracted from [Starline](https://starline-online.ru/)
- Manage technical services

### Tech

TaxiAdmin consists of two modules:

*  Frontend - ASP.NET, AngularJS
*  Scrapper ([repository](https://bitbucket.org/Trixic/starline/src/master/)) - CasperJS, Azure Tables
    * Scrapper module extracts data from monitoring service ([Starline](https://starline-online.ru/)) which GPS tracking devices installed in the vehicles
