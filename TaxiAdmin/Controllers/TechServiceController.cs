﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using TaxiAdmin.Data;

namespace TaxiAdmin.Controllers
{
    public class TechServiceController : ApiController
    {
        private readonly ITaxiAdminRepository _repo;

        public TechServiceController(ITaxiAdminRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<CarTechService> Get(string carId)
        {
            return _repo.GetCarTechServices(carId);
        }

        public HttpResponseMessage Post([FromBody] CarTechService techService)
        {
            if (_repo.AddCarTechService(techService))
            {
                return Request.CreateResponse(HttpStatusCode.Created, techService);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }

        public HttpResponseMessage Delete(string carId, string date)
        {
            var techService = new CarTechService()
            {
                car = carId,
                date = date
            };
            if (_repo.DeleteCarTechService(techService))
            {
                return Request.CreateResponse(HttpStatusCode.OK);
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest);
        }
    }
}
