﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using TaxiAdmin.Data;

namespace TaxiAdmin.Controllers
{
    public class StatsController : ApiController
    {
        private const string Format = "yyyy-MM-dd";
        private readonly ITaxiAdminRepository _repo;

        public StatsController(ITaxiAdminRepository repo)
        {
            _repo = repo;
        }

        public IEnumerable<CarDailyStat> Get(string start, string end, string carId = null)
        {
            return _repo.GetCarDailyStats(start, end, carId);
            //return _repo.GetCarDailyStats(DateTime.Now.AddDays(-7).ToString(Format), DateTime.Now.ToString(Format));
        }
    }
}
