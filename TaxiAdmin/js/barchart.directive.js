﻿angular
    .module("app")
    .directive("barchart", barchartDirective);

function barchartDirective() {

    return {

        // required to make it work as an element
        restrict: "E",
        template: "<div></div>",
        replace: true,
        // observe and manipulate the DOM
        link: function ($scope, element, attrs) {

            var data = $scope.$eval(attrs.data),
                xkey = $scope[attrs.xkey],
                ykeys = $scope[attrs.ykeys],
                labels = $scope[attrs.labels];

            Morris.Bar({
                element: element,
                barColors: ["grey"],
                data: data,
                xkey: xkey,
                ykeys: ykeys,
                labels: labels,
                hideHover: "auto",
                ymax: 500,
                postUnits: " km",
                xLabelFormat: function (x) {
                    return new Date(x.label).toLocaleDateString();
                    //return x.label.slice(-2);
                },
                resize: true
            });

        }

    };

}