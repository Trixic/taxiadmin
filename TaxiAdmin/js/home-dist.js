﻿angular
    .module("app")
    .controller("distController", distController);

distController.$inject = ["$scope", "carStatsService"];

angular
    .module("app")
    .config(function ($routeProvider) {
        $routeProvider.when("/", {
            controller: "distController",
            templateUrl: "/templates/distView.html"
        });

        $routeProvider.otherwise({ redirectTo: "/" });
    });

function distController($scope, carStatsService) {

    $scope.carsData = [];
    $scope.isBusy = true;
    $scope.xkey = "date";
    $scope.ykeys = ["total"];
    $scope.labels = ["Total Distance"];

    var date = new Date();

    $scope.periodOptions = [
        { id: "week", name: "7 дней" },
        { id: "month", name: "Текущий месяц" },
        { id: "month-1", name: new Date(date.getFullYear(), date.getMonth() - 1, 1).toLocaleString("ru", { month: "long", year: "numeric" }) },
        { id: "month-2", name: new Date(date.getFullYear(), date.getMonth() - 2, 1).toLocaleString("ru", { month: "long", year: "numeric" }) },
        { id: "month-3", name: new Date(date.getFullYear(), date.getMonth() - 3, 1).toLocaleString("ru", { month: "long", year: "numeric" }) }
    ];

    $scope.periodUpdate = function () {

        var promise = null;

        switch ($scope.selectedPeriodOption) {
            case "week":
                var startDate = new Date(Date.now() - 7 * 24 * 60 * 60 * 1000);
                var endDate = new Date(Date.now() - 24 * 60 * 60 * 1000);
                promise = carStatsService.getCarStats(startDate, endDate);
                break;
            case "month":
                promise = carStatsService.getMonthCarStats(0);
                break;
            case "month-1":
                promise = carStatsService.getMonthCarStats(1);
                break;
            case "month-2":
                promise = carStatsService.getMonthCarStats(2);
                break;
            case "month-3":
                promise = carStatsService.getMonthCarStats(3);
                break;
            default:
        }

        promise
        .then(function (carsData) {
            angular.copy(carsData, $scope.carsData);
            $scope.isBusy = false;
        });
    }

    $scope.selectedPeriodOption = "month-1";
    $scope.periodUpdate();
}