﻿angular
    .module("app")
    .directive("dateTimePicker", dateTimePickerDirective);

function dateTimePickerDirective() {

	return {
		restrict: "A",
		require: "ngModel",
		link: function (scope, element, attrs, ngModelCtrl) {
			var parent = $(element).parent();
			var dtp = parent.datetimepicker({
				format: "LL",
				//locale: "ru", todo:ngModel use different view and model values
				showTodayButton: true,
				maxDate: Date.now()
			});
			dtp.on("dp.change", function (e) {
				ngModelCtrl.$setViewValue(moment(e.date).format("LL"));
				scope.$apply();
			});
		}
	};

}