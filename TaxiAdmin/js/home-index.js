﻿angular
    .module("app")
    .controller("carStatsController", carStatsController);

angular
    .module("app")
    .controller("techServiceController", techServiceController);

carStatsController.$inject = ["$scope", "carStatsService"];
techServiceController.$inject = ["$scope", "$http", "$routeParams"];

angular
    .module("app")
    .config(function ($routeProvider) {
        $routeProvider.when("/", {
            controller: "carStatsController",
            templateUrl: "/templates/carStatsView.html"
        });

        $routeProvider.when("/techService/:carId", {
            controller: "techServiceController",
            templateUrl: "/templates/techServiceView.html"
        });

        $routeProvider.otherwise({ redirectTo: "/" });
    });

function carStatsController($scope, carStatsService) {

    $scope.carsData = [];
    $scope.isBusy = true;
    $scope.xkey = "date";
    $scope.ykeys = ["total"];
    $scope.labels = ["Total Distance"];

    var month = new Date().getDate() === 1 ? 1 : 0;

    carStatsService.getMonthCarStats(month, true)
        .then(function(carsData) {
            angular.copy(carsData, $scope.carsData);
            $scope.isBusy = false;
        });
}

function techServiceController($scope, $http, $routeParams) {

    var load = function () {
        $scope.car = $routeParams.carId;
        $scope.newTechService = {};
        $scope.newTechServiceDate = null;
        $scope.isBusy = true;

        $http.get("/api/techService?carId=" + $routeParams.carId)
            .then(function (result) {
                $scope.techServices = [];
                angular.copy(result.data.sort().reverse(), $scope.techServices);
                $scope.isBusy = false;
            });
    };

    load();

    $scope.addTechService = function () {

        $scope.newTechService.car = $routeParams.carId;
        $scope.newTechService.date = getDateString(new Date($scope.newTechServiceDate));
        $http.post("/api/techService", $scope.newTechService)
            .then(function success() {
                load();
            }, function fail() {
                alert("Ошибка!");
                load();
            });
    };

    $scope.removeTechService = function (techService) {

        bootbox.confirm({
            message: "Удалить запись?",
            backdrop: true,
            callback: function (result) {
                if (result) {
                    $http.delete("/api/techService", { params: { carId: techService.car, date: techService.date } })
                        .then(function () {
                            load();
                        });
                }
            }
        });
    };
}