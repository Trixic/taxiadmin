﻿angular
    .module("app")
    .factory("carStatsService", carStatsService);

carStatsService.$inject = ["$http", "$q"];

function carStatsService($http, $q) {

    var service = {
        getCarStats: getCarStats,
        getMonthCarStats: getMonthCarStats
    };

    return service;

    ////////////

    function fillCarData(carData, startDate, endDate) {

        var result = [];

        for (var d = new Date(startDate) ; d <= endDate; d.setDate(d.getDate() + 1)) {
            var dateString = getDateString(d);

            var found = carData.find(function (carRecord) {
                return carRecord.date === dateString;
            });

            if (found) {
                result.push(found);
            } else {
                result.push(
                {
                    "date": dateString,
                    "total": null
                });
            }
        }

        return result;
    }

    function getLastServiceInfo(car) {

        var deferred = $q.defer();

        $http.get("/api/techService?carId=" + car)
            .then(function (result) {
                var techServices = result.data.filter(r => !r.isNotRegular);

                if (techServices.length == 0) {
                    deferred.resolve({
                        date: "Нет данных",
                        passedDays: "-",
                        passedDist: "-"
                    });
                } else {

                    var lastService = techServices.sort(function (a, b) {
                        if (a.date < b.date)
                            return -1;
                        if (a.date > b.date)
                            return 1;
                        return 0;
                    }).pop();

                    $http.get("/api/stats", {
                        params: {
                            start: lastService.date,
                            end: getDateString(new Date()),
                            carId: car
                        }
                    })
                        .then(function (r) {
                            deferred.resolve({
                                date: lastService.date,
                                passedDays: moment().diff(moment(lastService.date), "days"),
                                passedDist: getTotalSum(r.data)
                            });
                        });
                }
            });

        return deferred.promise;
    }

    function getCarStats(startDate, endDate, includeTechServiceInfo) {

        var deferred = $q.defer();

        var params = "start=" + getDateString(startDate)
                     + "&end=" + getDateString(endDate);

        $http.get("/api/stats?" + params)
        .then(function (result) {

            var cars = result.data.reduce(function (acc, item) {
                var key = item.car;
                acc[key] = acc[key] || [];
                acc[key].push(item);
                return acc;
            }, {});

            var carsData = [];
            var promises = [];

            Object.keys(cars)
                .forEach(function (car) {

                    var lastService = includeTechServiceInfo ? getLastServiceInfo(car) : null;

                    var carGraphData = fillCarData(cars[car], startDate, endDate);

                    var weekTotal = getTotalSum(cars[car]);

                    promises.push(
                        $q.when(lastService)
                        .then(function (r) {
                            carsData.push({
                                number: car,
                                weekTotal: weekTotal,
                                graphData: carGraphData,
                                lastService: r
                            });
                        }));
                });

            $q.all(promises)
                .then(function () {
                    deferred.resolve(carsData.sort(function (a, b) {
                        return a.number.localeCompare(b.number);
                    }));
                });
        });

        return deferred.promise;
    }

    function getMonthCarStats(month, includeTechServiceInfo) {
        var date = new Date();
        var startDate = new Date(date.getFullYear(), date.getMonth() - month, 1);
        var endDate = new Date(date.getFullYear(), date.getMonth() - month + 1, 0);
        return getCarStats(startDate, endDate, includeTechServiceInfo);
    }
}

function getDateString(date) {
    return date.getFullYear() + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + ("0" + date.getDate()).slice(-2);
}

function getTotalSum(ar) {
    return ar.reduce(function (acc, x) {
        return acc += x.total;
    }, 0).toFixed();
}





