﻿using System.Collections.Generic;

namespace TaxiAdmin.Data
{
    public interface ITaxiAdminRepository
    {
        IEnumerable<CarDailyStat> GetCarDailyStats(string startDate, string endDate, string carId = null);

        IEnumerable<CarTechService> GetCarTechServices(string carId);

        bool AddCarTechService(CarTechService techService);

        bool DeleteCarTechService(CarTechService techService);
    }
}