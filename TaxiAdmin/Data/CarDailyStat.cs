﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.WindowsAzure.Storage.Table;
// ReSharper disable InconsistentNaming

namespace TaxiAdmin.Data
{
    public class CarDailyStat : TableEntity
    {
        public string car { get; set; }
        public string date { get; set; }
        public double total { get; set; }
        public double weekTotal { get; set; }
    }
}