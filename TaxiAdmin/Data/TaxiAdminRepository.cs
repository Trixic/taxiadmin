﻿using System;
using System.Collections.Generic;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace TaxiAdmin.Data
{
    public class TaxiAdminRepository : ITaxiAdminRepository
    {
        private const string DailyStatsPartitionKey = "stats";
        private const string TechServicePartitionKey = "techService";

        private static CloudTable GetTable()
        {
            var storageAccount = CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("TaxiStorage"));
            var tableClient = storageAccount.CreateCloudTableClient();
            return tableClient.GetTableReference("DailyStats");
        }

        public IEnumerable<CarDailyStat> GetCarDailyStats(string startDate, string endDate, string carId = null)
        {
            var table = GetTable();

            var filter = carId != null
                ? TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.GreaterThanOrEqual, string.Format("{0}-{1}", carId, startDate)),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.LessThanOrEqual, string.Format("{0}-{1}", carId, endDate)))
                : TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("date", QueryComparisons.GreaterThanOrEqual, startDate),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("date", QueryComparisons.LessThanOrEqual, endDate));

            var query = new TableQuery<CarDailyStat>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, DailyStatsPartitionKey),
                    TableOperators.And,
                    filter));


            return table.ExecuteQuery(query);
        }

        public IEnumerable<CarTechService> GetCarTechServices(string carId)
        {
            var table = GetTable();

            var query = new TableQuery<CarTechService>().Where(
                TableQuery.CombineFilters(
                    TableQuery.GenerateFilterCondition("PartitionKey", QueryComparisons.Equal, TechServicePartitionKey),
                    TableOperators.And,
                    TableQuery.GenerateFilterCondition("car", QueryComparisons.Equal, carId)));

            return table.ExecuteQuery(query);
        }

        public bool AddCarTechService(CarTechService techService)
        {
            var table = GetTable();

            techService.PartitionKey = TechServicePartitionKey;
            techService.RowKey = string.Format("{0}-{1}", techService.car, techService.date);

            table.Execute(TableOperation.Insert(techService));

            return true;
        }

        public bool DeleteCarTechService(CarTechService techService)
        {
            var table = GetTable();

            techService.PartitionKey = TechServicePartitionKey;
            techService.RowKey = string.Format("{0}-{1}", techService.car, techService.date);
            techService.ETag = "*";

            table.Execute(TableOperation.Delete(techService));

            return true;
        }
    }
}