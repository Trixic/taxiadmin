﻿using Microsoft.WindowsAzure.Storage.Table;
// ReSharper disable InconsistentNaming

namespace TaxiAdmin.Data
{
    public class CarTechService : TableEntity
    {
        public string car { get; set; }
        public string date { get; set; }
        public bool isNotRegular { get; set; }
        public string comment { get; set; }
    }
}