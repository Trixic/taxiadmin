﻿using System;
using System.Collections.Generic;

namespace TaxiAdmin.Data
{
    class FakeTaxiAdminRepository : ITaxiAdminRepository
    {
        private const string Format = "yyyy-MM-dd";

        public IEnumerable<CarDailyStat> GetCarDailyStats(string startDate, string endDate, string carId = null)
        {
            return new[]
            {
                new CarDailyStat()
                {
                    car = "carNum",
                    date = DateTime.Now.AddDays(-1).ToString(Format),
                    total = 100
                },
                new CarDailyStat()
                {
                    car = "carNum",
                    date = DateTime.Now.ToString(Format),
                    total = 150
                },
                new CarDailyStat()
                {
                    car = "carNum2",
                    date = DateTime.Now.AddDays(-1).ToString(Format),
                    total = 100
                },
                new CarDailyStat()
                {
                    car = "carNum2",
                    date = DateTime.Now.ToString(Format),
                    total = 100
                },
            };
        }

        public IEnumerable<CarTechService> GetCarTechServices(string carId)
        {
            return new[]
            {
                new CarTechService
                {
                    car = "carNum",
                    date = DateTime.Now.AddDays(-30).ToString(Format),
                    comment = "comment 1"
                },
                new CarTechService
                {
                    car = "carNum2",
                    date = DateTime.Now.AddDays(-10).ToString(Format),
                    comment = "comment 2"
                }
            };
        }

        public bool AddCarTechService(CarTechService techService)
        {
            return true;
        }

        public bool DeleteCarTechService(CarTechService techService)
        {
            return true;
        }
    }
}